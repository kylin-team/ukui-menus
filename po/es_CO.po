# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Daniel Aranda <dwarandae@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: UKUI Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-07-01 16:24+0300\n"
"PO-Revision-Date: 2016-08-21 00:18+0000\n"
"Last-Translator: Daniel Aranda <dwarandae@gmail.com>\n"
"Language-Team: Spanish (Colombia) (http://www.transifex.com/ukui/UKUI/language/es_CO/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: es_CO\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../desktop-directories/ukui-audio-video.directory.in.h:1
msgid "Sound & Video"
msgstr "Audio y Video"

#: ../desktop-directories/ukui-audio-video.directory.in.h:2
msgid "Multimedia menu"
msgstr "Menú multimedia"

#: ../desktop-directories/ukui-development.directory.in.h:1
msgid "Programming"
msgstr ""

#: ../desktop-directories/ukui-development.directory.in.h:2
msgid "Tools for software development"
msgstr "Herramientas para el desarrollo de software"

#: ../desktop-directories/ukui-education.directory.in.h:1
msgid "Education"
msgstr "Educación"

#: ../desktop-directories/ukui-game.directory.in.h:1
msgid "Games"
msgstr "Juegos"

#: ../desktop-directories/ukui-game.directory.in.h:2
msgid "Games and amusements"
msgstr ""

#: ../desktop-directories/ukui-graphics.directory.in.h:1
msgid "Graphics"
msgstr ""

#: ../desktop-directories/ukui-graphics.directory.in.h:2
msgid "Graphics applications"
msgstr ""

#: ../desktop-directories/ukui-hardware.directory.in.h:1
msgid "Hardware"
msgstr ""

#: ../desktop-directories/ukui-hardware.directory.in.h:2
msgid "Settings for several hardware devices"
msgstr ""

#: ../desktop-directories/ukui-internet-and-network.directory.in.h:1
msgid "Internet and Network"
msgstr ""

#: ../desktop-directories/ukui-internet-and-network.directory.in.h:2
msgid "Network-related settings"
msgstr ""

#: ../desktop-directories/ukui-look-and-feel.directory.in.h:1
msgid "Look and Feel"
msgstr ""

#: ../desktop-directories/ukui-look-and-feel.directory.in.h:2
msgid "Settings controlling the desktop appearance and behavior"
msgstr ""

#: ../desktop-directories/ukui-network.directory.in.h:1
msgid "Internet"
msgstr ""

#: ../desktop-directories/ukui-network.directory.in.h:2
msgid "Programs for Internet access such as web and email"
msgstr ""

#: ../desktop-directories/ukui-office.directory.in.h:1
msgid "Office"
msgstr ""

#: ../desktop-directories/ukui-office.directory.in.h:2
msgid "Office Applications"
msgstr ""

#. Translators: this is Personal as in "Personal settings"
#: ../desktop-directories/ukui-personal.directory.in.h:2
msgid "Personal"
msgstr ""

#: ../desktop-directories/ukui-personal.directory.in.h:3
msgid "Personal settings"
msgstr ""

#: ../desktop-directories/ukui-settings-system.directory.in.h:1
msgid "Administration"
msgstr ""

#: ../desktop-directories/ukui-settings-system.directory.in.h:2
msgid "Change system-wide settings (affects all users)"
msgstr ""

#: ../desktop-directories/ukui-settings.directory.in.h:1
msgid "Preferences"
msgstr ""

#: ../desktop-directories/ukui-settings.directory.in.h:2
msgid "Personal preferences"
msgstr ""

#: ../desktop-directories/ukui-system.directory.in.h:1
#: ../desktop-directories/ukui-menu-system.directory.in.h:1
msgid "System"
msgstr "Sistema"

#: ../desktop-directories/ukui-system.directory.in.h:2
msgid "System settings"
msgstr ""

#: ../desktop-directories/ukui-system-tools.directory.in.h:1
msgid "System Tools"
msgstr ""

#: ../desktop-directories/ukui-system-tools.directory.in.h:2
msgid "System configuration and monitoring"
msgstr ""

#: ../desktop-directories/ukui-utility-accessibility.directory.in.h:1
msgid "Universal Access"
msgstr ""

#: ../desktop-directories/ukui-utility-accessibility.directory.in.h:2
msgid "Universal Access Settings"
msgstr ""

#: ../desktop-directories/ukui-utility.directory.in.h:1
msgid "Accessories"
msgstr ""

#: ../desktop-directories/ukui-utility.directory.in.h:2
msgid "Desktop accessories"
msgstr ""

#: ../desktop-directories/ukui-menu-applications.directory.in.h:1
msgid "Applications"
msgstr ""

#: ../desktop-directories/ukui-menu-system.directory.in.h:2
msgid "Personal preferences and administration settings"
msgstr ""

#: ../desktop-directories/ukui-other.directory.in.h:1
msgid "Other"
msgstr ""

#: ../desktop-directories/ukui-other.directory.in.h:2
msgid "Applications that did not fit in other categories"
msgstr ""

#: ../desktop-directories/ukui-android.directory.in.h:1
msgid "Android"
msgstr ""

#: ../desktop-directories/ukui-android.directory.in.h:2
msgid "Android compatible application"
msgstr ""
