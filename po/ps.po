# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: UKUI Desktop Environment\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-07-01 16:24+0300\n"
"PO-Revision-Date: 2012-07-04 13:47+0000\n"
"Last-Translator: Stefano Karapetsas <stefano@karapetsas.com>\n"
"Language-Team: Pushto (http://www.transifex.com/ukui/UKUI/language/ps/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ps\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../desktop-directories/ukui-audio-video.directory.in.h:1
msgid "Sound & Video"
msgstr "غږ او وېډيو"

#: ../desktop-directories/ukui-audio-video.directory.in.h:2
msgid "Multimedia menu"
msgstr "ګڼرسنۍ غورنۍ"

#: ../desktop-directories/ukui-development.directory.in.h:1
msgid "Programming"
msgstr "پروګرامونه"

#: ../desktop-directories/ukui-development.directory.in.h:2
msgid "Tools for software development"
msgstr "د ساوترو جوړولو لپاره توکي"

#: ../desktop-directories/ukui-education.directory.in.h:1
msgid "Education"
msgstr "زدکړه"

#: ../desktop-directories/ukui-game.directory.in.h:1
msgid "Games"
msgstr "لوبې"

#: ../desktop-directories/ukui-game.directory.in.h:2
msgid "Games and amusements"
msgstr "لوبې او مهالتيري"

#: ../desktop-directories/ukui-graphics.directory.in.h:1
msgid "Graphics"
msgstr "کښنيزونه"

#: ../desktop-directories/ukui-graphics.directory.in.h:2
msgid "Graphics applications"
msgstr "کښنيزونو کاريالونه"

#: ../desktop-directories/ukui-hardware.directory.in.h:1
msgid "Hardware"
msgstr "هډوتري"

#: ../desktop-directories/ukui-hardware.directory.in.h:2
msgid "Settings for several hardware devices"
msgstr "د ډېرو هډوتري وزلو لپاره امستنې"

#: ../desktop-directories/ukui-internet-and-network.directory.in.h:1
msgid "Internet and Network"
msgstr "اېنټرنېټ او ځال"

#: ../desktop-directories/ukui-internet-and-network.directory.in.h:2
msgid "Network-related settings"
msgstr "ځال پورې تړلې امستنې"

#: ../desktop-directories/ukui-look-and-feel.directory.in.h:1
msgid "Look and Feel"
msgstr "ښکارېدنه"

#: ../desktop-directories/ukui-look-and-feel.directory.in.h:2
msgid "Settings controlling the desktop appearance and behavior"
msgstr "د سرپاڼې د ښکارېدو او کړه وړې امستنې"

#: ../desktop-directories/ukui-network.directory.in.h:1
msgid "Internet"
msgstr "اېنټرنېټ"

#: ../desktop-directories/ukui-network.directory.in.h:2
msgid "Programs for Internet access such as web and email"
msgstr "د اېنټرنېټ د لاسرس لپاره کړنلارې لکه ګورت او برېښليک"

#: ../desktop-directories/ukui-office.directory.in.h:1
msgid "Office"
msgstr "افس"

#: ../desktop-directories/ukui-office.directory.in.h:2
msgid "Office Applications"
msgstr "د افس کاريالونه"

#. Translators: this is Personal as in "Personal settings"
#: ../desktop-directories/ukui-personal.directory.in.h:2
msgid "Personal"
msgstr "وګړيز"

#: ../desktop-directories/ukui-personal.directory.in.h:3
msgid "Personal settings"
msgstr "وګړيزې امستنې"

#: ../desktop-directories/ukui-settings-system.directory.in.h:1
msgid "Administration"
msgstr "پازوالنه"

#: ../desktop-directories/ukui-settings-system.directory.in.h:2
msgid "Change system-wide settings (affects all users)"
msgstr "(په ټولو کارنانو کارول کيږي) غونډال-ارت امستنې بدلول"

#: ../desktop-directories/ukui-settings.directory.in.h:1
msgid "Preferences"
msgstr "غوراوي"

#: ../desktop-directories/ukui-settings.directory.in.h:2
msgid "Personal preferences"
msgstr "وکړيز غوراوي"

#: ../desktop-directories/ukui-system.directory.in.h:1
#: ../desktop-directories/ukui-menu-system.directory.in.h:1
msgid "System"
msgstr "غونډال"

#: ../desktop-directories/ukui-system.directory.in.h:2
msgid "System settings"
msgstr "غونډال امستنې"

#: ../desktop-directories/ukui-system-tools.directory.in.h:1
msgid "System Tools"
msgstr "غونډال توکي"

#: ../desktop-directories/ukui-system-tools.directory.in.h:2
msgid "System configuration and monitoring"
msgstr "د غونډال سازونه او ليدنه"

#: ../desktop-directories/ukui-utility-accessibility.directory.in.h:1
msgid "Universal Access"
msgstr "نړېوال لاسرس"

#: ../desktop-directories/ukui-utility-accessibility.directory.in.h:2
msgid "Universal Access Settings"
msgstr "د نړېوال لاسرس امستنې"

#: ../desktop-directories/ukui-utility.directory.in.h:1
msgid "Accessories"
msgstr "ملتوکي"

#: ../desktop-directories/ukui-utility.directory.in.h:2
msgid "Desktop accessories"
msgstr "د سرپاڼې ملتوکي"

#: ../desktop-directories/ukui-menu-applications.directory.in.h:1
msgid "Applications"
msgstr "کاريالونه"

#: ../desktop-directories/ukui-menu-system.directory.in.h:2
msgid "Personal preferences and administration settings"
msgstr "وګړيز غوراوي او د پازوالنې امستنې"

#: ../desktop-directories/ukui-other.directory.in.h:1
msgid "Other"
msgstr "نور"

#: ../desktop-directories/ukui-other.directory.in.h:2
msgid "Applications that did not fit in other categories"
msgstr "هغه کاريالونه چې په نورو ټولېو کې نه راځي"

#: ../desktop-directories/ukui-android.directory.in.h:1
msgid "Android"
msgstr ""

#: ../desktop-directories/ukui-android.directory.in.h:2
msgid "Android compatible application"
msgstr ""
